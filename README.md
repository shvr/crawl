crawl - A Python(3.x) utility

crawl is developed to challenge my knowledge of programming and computer science
in general. Early intentions are to be able to traverse a filesystem from a given
starting point and given depth. crawl should be able to distinguish files from
folders and list this information in some way.

The only purpose is to show how to navigate through directories given only a
parent directory which contains both files and sub-directiories.