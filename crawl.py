#!/usr/bin/env python3

## crawl - A Python(3.x) utility to traverse a filesystem
#  Functional implementation

import os
import sys

def get_contents(check_dir):

    '''Pull contents with their real path out of check_dir.'''
    content_list = []
    for item in os.listdir(check_dir):
        content_list.append(check_dir + os.sep + item)
    return content_list


def crawl(directory, sorted=True):

    '''Return a list of directory contents.'''
    work_dir = directory
    dir_tree = []
    queue = []
    no_more_dirs = False

    while no_more_dirs == False:
        # Any contents?
        try:
            if os.listdir(work_dir):
                queue.append(get_contents(work_dir))
        except (NotADirectoryError, PermissionError):
            pass
        # Loop a copy so we can modify original
        for item in queue[-1][:]:
            if os.path.isfile(item) or os.path.islink(item):
                dir_tree.append(queue[-1].pop(queue[-1].index(item)))
        # Exit if queue is empty otherwise continue.
        if len(queue[-1]) == 0 and queue[-1] == queue[0]:
            no_more_dirs = True
            break
        else:
            # Purge empty lists
            while [] in queue:
                queue.remove([])
            # Move to next in queue + store in dir_tree
            work_dir = queue[-1].pop(0)
            dir_tree.append(work_dir)

    if sorted:
        dir_tree.sort()

    return dir_tree


if __name__ == '__main__':

    # Get directory or exit
    for arg in sys.argv:
        # Use first found
        if os.path.lexists(arg) and os.path.isdir(arg):
            root_dir = os.path.normpath(arg)
            break
    else:
        print('Directory not specified / found.')
        sys.exit(0)

    if os.listdir(root_dir) == []:
        print('%s is empty.' % root_dir)
        sys.exit(0)

    # Reformat the help message eventually
    # Switch to argparse or commandopt-lib should any work be done there
    command_opts = ['-h', '--help', '--unsorted', '-u']
    if '-h' in sys.argv or '--help' in sys.argv:
        print("crawl usage: crawl <opts> [directory] \nOptions: \n    -u, --unsorted      Don't sort output \n    -h, --help          Show help message and exit")
        sys.exit(0)

    if '-u' in sys.argv or '--unsorted' in sys.argv:
        do_sort = False
    else:
        do_sort = True
    
    for content in crawl(root_dir, sorted = do_sort):
        print(content)
